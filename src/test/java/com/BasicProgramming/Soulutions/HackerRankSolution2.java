package com.BasicProgramming.Soulutions;

/*
 * Two Strings are anagrams if they are permutation of each other.
 * Given array of a String, Remove each String that is an Anagram of an earlier String
 * Then return remaining array in sorted order
 */
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import com.BasicProgramming.Runners.AbstractHacker;

public class HackerRankSolution2 extends AbstractHacker {

	public HackerRankSolution2() {
		// To enter elements in a list
		List<String> list = new ArrayList<String>();
		Scanner sc = new Scanner(System.in);
		System.out.println("Please enter the size of list");
		int listSize = sc.nextInt();
		System.out.println("Enter the list elements");
		for (int i = 0; i <= listSize; i++) {
			String listElement = sc.next();
			list.add(listElement);
		}
		TestAnagram ana = new TestAnagram();
		ana.findAndSortList(list);
		System.out.println(list);
	}
}

class TestAnagram {
	public static List<String> findAndSortList(List<String> text) {
		int size = text.size();
		for (int i = 0; i < size; i++) {
			for (int j = i + 1; i < size + 1; j++) {
				if (checkAnagram(text.get(i), text.get(j))) {
					text.remove(text.get(j));
				}
			}
		}
		return text;

	}

	private static boolean checkAnagram(String s1, String s2) {
		if (s1.length() != s2.length()) {
			return false;
		}
		// return (boolean) (s1.length()!=s2.length()?0:false);

		char[] s1Characters = s1.toLowerCase().toCharArray();
		Arrays.sort(s1Characters);
		char[] s2Characters = s2.toLowerCase().toCharArray();
		Arrays.sort(s2Characters);
		if (!Arrays.equals(s1Characters, s2Characters)) {
			return false;
		}
		return true;

	}
}