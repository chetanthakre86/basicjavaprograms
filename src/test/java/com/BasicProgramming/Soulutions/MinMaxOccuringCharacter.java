/*Grass is greener on the other side
 * In above example, character 'a' is occurred only once in the string. So, it is minimum occurring character 
 *Character e has occurred maximum number of times in the entire string i.e. 6 times.
 * */
package com.BasicProgramming.Soulutions;

import java.util.Scanner;

public class MinMaxOccuringCharacter {
	Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		String input2 = "Grass is greener";

		Solutions sol = new Solutions();
		sol.characterCount(input2);
	}

}

class Solutions {
	public Solutions() {

	}

	protected void characterCount(String input) {
		String[] stringArray = input.split(" ");
		char[] inputArray = stringArray.toString().toCharArray();
		int len=inputArray.length;
		int[] freq = new int[inputArray.length];
		int max, min;
		char minChar = input.charAt(0);
		char maxChar = input.charAt(0);
		for (int i = 0; i < inputArray.length; i++) {
			freq[i] = 1;
			for (int j = i + 1; j < inputArray.length; j++) {
				if (inputArray[i] == inputArray[j] && inputArray[i] != ' ' && inputArray[i] != '0') {
					freq[i]++;
					inputArray[j] = '0';
				}
			}
		}
		min = max = freq[0];
		for (int k = 0; k < freq.length; k++) {
			if (min > freq[k] && freq[k] != 0) {
				min = freq[k];
				minChar = inputArray[k];
			}
			if (max < freq[k] && freq[k] != 0) {
				max = freq[k];
				maxChar = inputArray[k];
			}
		}
		System.out.println("Minimum Character Repatation is " + minChar + " at position " + min);
		System.out.println("Maximum Character Repetation is " + maxChar + " at position " + max);
	}

}
