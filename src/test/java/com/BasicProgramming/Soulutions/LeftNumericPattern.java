package com.BasicProgramming.Soulutions;

import com.BasicProgramming.Runners.StarAndNumericPattern;

/**
 * @author Sanyu
 *
 */
public class LeftNumericPattern extends StarAndNumericPattern {

	public LeftNumericPattern() {
		printPattern();
		printPattern1();
		printPattern2();
		printPattern3();
	}

	public void printPattern() {
		int k = -1;
		for (int i = 0; i <= noOfRows; i++) {
			for (int j = 0; j <= i; j++) {
				k = k + 2;
				System.out.print(k + " ");
			}
			System.out.println(" ");
		}
	}

	public void printPattern1() {
		System.out.println("##########################################");
		for (int i = 1; i <= noOfRows; i++) {
			for (int j = 1; j <= i; j++) {
				System.out.print(i + " ");
			}
			System.out.println(" ");
		}
	}

	public void printPattern2() {
		System.out.println("#######################################");
		for (int i = 0; i <= noOfRows; i++) {
			for (int j = 0; j <= i; j++) {
				System.out.print(((i + j) % 2) + " ");
			}
			System.out.println(" ");
		}
	}

	public void printPattern3() {
		System.out.println("#######################################");
		int k = 1;
		for (int i = 0; i <= noOfRows; i++) {
			for (int j = 0; j <= i; j++) {
				System.out.print(k + " ");
				k++;
			}
			System.out.println(" ");
		}
	}

}
