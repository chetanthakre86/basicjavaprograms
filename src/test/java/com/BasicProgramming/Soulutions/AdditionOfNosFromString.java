package com.BasicProgramming.Soulutions;

import java.util.Scanner;

import com.BasicProgramming.Runners.AbstractStringProgram;

/**
 * @author Sanyu Example: 12sdsf45gh0hgg8d3ad00 Output of
 *         findSumIndividualDigits(input) :Sum of individual digits in a string
 *         is :23 Output of findSumAllDigits(input):Sum of all digits is 68
 */

public class AdditionOfNosFromString extends AbstractStringProgram {
	Scanner sc = new Scanner(System.in);

	public AdditionOfNosFromString() {
		System.out.println("Please enter the string for string digits addtion");
		String input = sc.nextLine();
		findSumIndividualDigits(input);
		findSumAllDigits(input);
	}

	private void findSumIndividualDigits(String input) {
		int sum = 0;
		for (int i = 0; i <= input.length() - 1; i++) {
			if (Character.isDigit(input.charAt(i))) {
				// System.out.print(input.charAt(i)+",");
				String tmp = input.valueOf(input.charAt(i));
				sum = sum + Integer.parseInt(tmp);
			}
		}
		System.out.println("Sum of individual digits in a string is :" + sum);
	}

	static void findSumAllDigits(String input) {
		String temp = "";
		int sum = 0;

		for (int j = 0; j < input.length(); j++) {
			char a = input.charAt(j);
			if (Character.isDigit(a)) {
				temp += a;
			} else {
				sum += Integer.parseInt(temp);
				temp = "0";
			}
		}
		sum = sum + Integer.parseInt(temp);
		System.out.println("Sum of all positional nos is " + sum);

	}
}
