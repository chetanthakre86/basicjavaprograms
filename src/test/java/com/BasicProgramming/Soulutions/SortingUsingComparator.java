package com.BasicProgramming.Soulutions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import com.BasicProgramming.Runners.AbstractSortingCollection;

/**
 * @author Sanyu
 *
 */
public class SortingUsingComparator extends AbstractSortingCollection {
	List<Employee> empList = new ArrayList<Employee>();

	public SortingUsingComparator() {
		prerequisite();

	}

	public void prerequisite() {

		empList.add(new Employee("Chetan", 50000));
		empList.add(new Employee("Sanyu", 4999));
		empList.add(new Employee("Thakre", 39999));

		for (int i = 0; i < empList.size(); i++) {
			System.out.println(empList.get(i).toString());

		}
		Collections.sort(empList, new SortByName());
		System.out.println("###Sorted List By Name###");

		for (int i = 0; i < empList.size(); i++) {
			System.out.println(empList.get(i));

		}

		Collections.sort(empList, new SortBySalary());

		System.out.println("###Sorted List By Salary###");
		for (int i = 0; i < empList.size(); i++) {
			System.out.println(empList.get(i));
		}
		Collections.reverse(empList);
		System.out.println("###Sorted List By Salary in a reverse order###");
		for (int i = 0; i < empList.size(); i++) {
			System.out.println(empList.get(i));
		}
	}

}

class Employee {
	String empName;
	int salary;

	public Employee(String empName, int salary) {
		// super();
		this.empName = empName;
		this.salary = salary;

	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary() {
		this.salary = salary;
	}

	public String toString() {
		return this.empName + " " + this.salary;
	}

}

// Comparator by using class implementation
class SortByName implements Comparator<Employee> {

	@Override
	public int compare(Employee emp, Employee emp1) {
		return emp.empName.compareTo(emp1.empName);

	}
}

class SortBySalary implements Comparator<Employee> {

	@Override
	public int compare(Employee o1, Employee o2) {

		return o1.salary - o2.salary;
	}

}
