package com.BasicProgramming.Soulutions;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class RetryFailTestCase implements IRetryAnalyzer {

	/*
	 * @Override public boolean retry(ITestResult result) { int counter=0; //
	 * TODO Auto-generated method stub RetryCountIffailed
	 * annotation=result.getMethod().getConstructorOrMethod().getMethod()
	 * .getAnnotation(RetryCountIffailed.class); if
	 * ((annotation!=null)&&(counter<annotation.value())) { counter++; return
	 * true; } return false; }
	 */

	// OR
	public boolean retry(ITestResult result) {
		// TODO Auto-generated method stub
		int counter = 0;
		int maxRetry = 3;
		if (counter < maxRetry) {
			counter++;
			return true;
		}
		return false;
	}

}
