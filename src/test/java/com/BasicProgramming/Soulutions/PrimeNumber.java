package com.BasicProgramming.Soulutions;

import java.util.Scanner;

class Prime {
	protected int number;

	Prime(int number) {
		this.number = number;
		isPrime(number);
	}

	void isPrime(int no) {
		int flag = 0;
		int m = no / 2;
		if (no == 0 || no == 1) {
			System.out.println("Not a prime no.");
			flag = 1;
		}
		for (int i = 2; i <= m; i++) {
			if (no % i == 0) {
				System.out.println("No. is not a prime no.");
				flag = 1;
				break;
			}
		}
		if (flag == 0) {
			System.out.println("no. is a prime no.");
		}
	}
}

public class PrimeNumber {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.println("Please enter the no. :");
		int num = sc.nextInt();
		Prime prime = new Prime(num);

	}

}
