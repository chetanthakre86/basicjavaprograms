package com.BasicProgramming.Soulutions;

import java.util.HashMap;
import java.util.Map;

import javax.swing.plaf.synth.SynthSeparatorUI;

import org.apache.commons.lang3.ArrayUtils;

import com.BasicProgramming.Runners.ArrayManipulation;

public class ArrayMapConversion extends ArrayManipulation {
	Map<String, String> hMap = new HashMap<String, String>();

	public ArrayMapConversion() {
		stringToHashMap();
	}

	public void stringToHashMap() {
		String str = "11:Jack, 23:Emily, 31:Ryan, 56:John, 99:Maria";
		String splitStr[] = str.split(",");
		for (String sb : splitStr) {
			String[] splitSb = sb.split(":");
			String no = splitSb[0];
			String name = splitSb[1];
			hMap.put(no, name);
		}
		System.out.println("Hasmap Conversion : " + hMap);

	}

	private void stringArrayToHashMap() {
		/*
		 * String str[] = { {11,Jack}, {23,Emily, 31:Ryan, 56:John, 99:Maria}; }
		 */

	}
}