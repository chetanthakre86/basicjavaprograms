package com.BasicProgramming.Soulutions;

import java.util.Arrays;
import java.util.Scanner;

import com.BasicProgramming.Runners.ArrayManipulation;

public class ArrayPrograms extends ArrayManipulation {
	Scanner sc = new Scanner(System.in);

	public ArrayPrograms() {
		sortingArray();
	}

	// To find biggest no in an array
	public void sortingArray() {
		// To take array input
		System.out.println("Please enter length of an array");
		int n = sc.nextInt();
		int a[] = new int[n];
		System.out.println("Please enter required array elements");

		try {
			for (int i = 0; i < n; i++) {
				try {
					a[i] = sc.nextInt();
				} catch (ArrayIndexOutOfBoundsException ex) {
					ex.printStackTrace();
				}
			}
		} catch (ArrayIndexOutOfBoundsException e) {

			e.printStackTrace();
		}
		System.out.println("Being used array is : " + Arrays.toString(a));
		Arrays.sort(a);
		System.out.println("Sorted Array using method : " + Arrays.toString(a));

		// Sorting without using Method
		for (int i = 0; i < n; i++) {
			for (int j = i + 1; j < n - 1; j++) {
				if (a[i] >= a[j]) {
					int tmp = a[j];
					a[i] = a[j];
					a[j] = a[i];
				}
			}
		}

		System.out.println("Sorted array is : ");
		for (int k = 0; k < n - 1; k++) {
			System.out.print(a[k] + ",");
		}
		System.out.print(a[n - 1]);

	}
}
