package com.BasicProgramming.Soulutions;

import com.BasicProgramming.Runners.StarAndNumericPattern;

/**
 * @author Sanyu
 *
 */
public class RightstarPattern extends StarAndNumericPattern {

	public RightstarPattern() {
		printRightStarPattern();
		printAllRightStarPattern();
	}

	public void printRightStarPattern() {
		int i, j;
		for (i = 0; i <= noOfRows; i++) {

			for (j = noOfRows; j > i; j--) {
				System.out.print(" ");
			}
			for (int k = 0; k <= i; k++) {
				System.out.print("*");
			}
			System.out.println();
		}
	}

	public void printAllRightStarPattern() {
		System.out.println("#########################");
		int i, j;
		for (i = 0; i <= noOfRows; i++) {
			for (j = noOfRows; j <= noOfRows - i; j++) {
				System.out.println("*");
			}
			System.out.print(" ");
		}
		System.out.println();
	}

}
