package com.BasicProgramming.Soulutions;

import java.io.IOException;
import java.util.Scanner;
/*PENDING
 * Complete the 'shortestPalindrome' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts STRING s as parameter.
 * add character to make palindrome
 */

import com.BasicProgramming.Runners.AbstractHacker;

public class HackerRankSolution6 extends AbstractHacker {
	public HackerRankSolution6() {
		Scanner sca = new Scanner(System.in);
		String s = sca.next();
		// String s = bufferedReader.readLine();

		int result = HackerRankSolution6.shortestPalindrome(s);
		System.out.println(result);
	}

	public static int shortestPalindrome(String s) {
		// Write your code here
		int i = 0;
		int count = 0;
		int j = s.length() - 1;
		int mid = s.length() / 2;
		System.out.println(mid);
		while (j >= 0) {
			if (s.charAt(i) != s.charAt(j)) {
				count++;
			} else {
				i++;
			}
			j--;
			if (i > mid && j < mid) {
				return count;
			}
		}
		return count;
	}
}
