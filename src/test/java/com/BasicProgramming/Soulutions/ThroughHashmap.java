package com.BasicProgramming.Soulutions;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class ThroughHashmap {

	public static void main(String[] args) {

		HashMapTest hashMap = new HashMapTest();
		System.out.println("=======Hash Map=============");
		hashMap.HashMapCheck();
		LinkHashMapTest linkHashMap = new LinkHashMapTest();
		System.out.println("============LinkHashMap==============");
		linkHashMap.LinkHashMapCheck();
		TreeMapTest treemap = new TreeMapTest();
		System.out.println("========Tree Map============");
		treemap.TreeMapCheck();
	}

}

class HashMapTest {
	Map<Integer, String> hmap = new HashMap<Integer, String>();

	public void HashMapCheck() {

		// Adding elements to HashMap
		hmap.put(11, "AB");
		hmap.put(2, "CD");
		hmap.put(33, "EF");
		hmap.put(9, "GH");
		hmap.put(3, "IJ");
		hmap.put(5, null);
		hmap.put(6, null);
		hmap.put(5, "overwrite null");
		hmap.put(null, "Kye is null");
		hmap.put(null, "Kye ");

		System.out.println("Using For loop:");
		int index = 1;
		for (Map.Entry me : hmap.entrySet()) {
			System.out.println(index + ") Key :" + " " + me.getKey() + ",Value" + ": " + me.getValue());
			index++;
		}
		index = 1;
		System.out.println("---------Via Set,EntrySey,map.entry-----------");
		Set entryPoint = hmap.entrySet();
		Iterator itr = entryPoint.iterator();
		while (itr.hasNext()) {
			Map.Entry entry = (Map.Entry) itr.next();

			System.out.println(index + ") Key :" + " " + entry.getKey() + ", Value: " + entry.getValue());
			index++;
		}

	}
}

class TreeMapTest {
	Map<Integer, String> treeMap = new TreeMap<Integer, String>();

	public void TreeMapCheck() {

		// Adding elements to HashMap
		treeMap.put(11, "AB");
		treeMap.put(2, "CD");
		treeMap.put(33, "EF");
		treeMap.put(9, "GH");
		treeMap.put(3, "IJ");
		treeMap.put(5, null);
		treeMap.put(6, null);
		treeMap.put(5, "overwrite null");
		treeMap.put(null, "Kye is null");
		

		System.out.println("Using For loop:");
		int index = 1;
		for (Map.Entry me : treeMap.entrySet()) {
			System.out.println(index + ") Key :" + " " + me.getKey() + ",Value" + ": " + me.getValue());
			index++;
		}

	}
}

class LinkHashMapTest {
	Map<Integer, String> hmap = new LinkedHashMap<Integer, String>();

	public void LinkHashMapCheck() {

		// Adding elements to HashMap
		hmap.put(11, "AB");
		hmap.put(2, "CD");
		hmap.put(33, "EF");
		hmap.put(9, "GH");
		hmap.put(3, "IJ");
		hmap.put(5, null);
		hmap.put(6, null);
		hmap.put(5, "overwrite null");
		hmap.put(null, "Kye is null");
		hmap.put(null, "Kye ");

		System.out.println("Using For loop:");
		int index = 1;
		for (Map.Entry me : hmap.entrySet()) {
			System.out.println(index + ") Key :" + " " + me.getKey() + ",Value" + ": " + me.getValue());
			index++;
		}
		index = 1;
		System.out.println("---------Via Set,EntrySey,map.entry-----------");
		Set entryPoint = hmap.entrySet();
		Iterator itr = entryPoint.iterator();
		while (itr.hasNext()) {
			Map.Entry entry = (Map.Entry) itr.next();

			System.out.println(index + ") Key :" + " " + entry.getKey() + ", Value: " + entry.getValue());
			index++;
		}

	}
}
