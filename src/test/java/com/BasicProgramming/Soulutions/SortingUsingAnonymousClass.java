package com.BasicProgramming.Soulutions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.BasicProgramming.Runners.AbstractSortingCollection;

public class SortingUsingAnonymousClass extends AbstractSortingCollection {
	List<Students> list = new ArrayList<Students>();

	public SortingUsingAnonymousClass() {
		setAndSortStudentDetails();
	}

	public void setAndSortStudentDetails() {
		list.add(new Students(120, "Chetan", "Pune"));
		list.add(new Students(101, "Sanyu", "Ngp"));
		list.add(new Students(501, "Thakre", "Mumbai"));
		sortByNameAnonymousClass();
	}

	public void sortByNameAnonymousClass() {
		Collections.sort(list, new Comparator<Students>() {

			@Override
			public int compare(Students s1, Students s2) {

				return s1.name.compareTo(s2.name);
			}

		});
		System.out.println("Sort By Name Using Anonymous Class");
		list.forEach(System.out::println);
	}
}

class Students {
	int rollNo;
	String name;
	String address;

	public Students(int rollNo, String name, String address) {
		this.rollNo = rollNo;
		this.name = name;
		this.address = address;
	}

	public int getRollNo() {
		return rollNo;

	}

	public void setRollNo(int rollNo) {
		this.rollNo = rollNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String toString() {
		return this.rollNo + " " + this.name + " " + this.address;
	}

}
