package com.BasicProgramming.Soulutions;
/*
 * Hiding the variables of a class from other classes and giving access to them 
 * only through method(getter and setter).Hence Encapsulation in java is binding 
 * a data (variable) and code together
 */

import java.util.Scanner;

import com.BasicProgramming.Runners.UpsProgrammingConcept;

public class EncapsulationConcept extends UpsProgrammingConcept {

	RunEncapsulation runCapusle;

	public EncapsulationConcept() {
		runCapusle = new RunEncapsulation();

	}

	private String capsuleName;

	public String getCapsule() {
		return capsuleName;

	}

	public void setCapsule(String capsule) {
		this.capsuleName = capsule;
	}
}

class RunEncapsulation {
	EncapsulationConcept concept = new EncapsulationConcept();
	Scanner sc = new Scanner(System.in);

	private void printEncapsul() {
		System.out.println("Please enter the capsule name");
		String name = sc.nextLine();
		concept.setCapsule(name);
		System.out.println(concept.getCapsule() + " : You will get.");

	}
}