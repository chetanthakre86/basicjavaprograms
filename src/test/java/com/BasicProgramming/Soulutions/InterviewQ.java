/**
 * 
 */
package com.BasicProgramming.Soulutions;

/**
 * @author A697651 static private method is not allowed outside the class
 */
public class InterviewQ {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Questions.basicQuestionSet();

	}

}

class Questions {
	int a;
	static int b;
	final int c; /*
					 * If we initialized or not initialized final variable and
					 * increment inside constructor it will give compilation
					 * error. If we does not initialized final variable and
					 * assigned value inside the constructor it will run fine.
					 */

	Questions() {
		a++;
		b++;
		// c++; //It will give compilation error
		c = 0;
	}

	static public void basicQuestionSet() {
		String a = "Sachin";
		int b = 1;
		String c = a + b;
		System.out.println(c);
		String d = b + a;
		System.out.println(d);
	}
}