package com.BasicProgramming.Soulutions;
/*
 * Letters in some of the SOS messages are altered by comic radiation during transmission. Given the signal received by Earth as a string, 
, determine how many letters of Sami's SOS have been changed by radiation.
For example, Earth receives SOSTOT. Sami's original message was SOSSOS. Two of the message characters were changed in transit. 
Function Description.Complete the marsExploration function in the editor below. It should return an integer representing the number of letters changed during transmission. 
marsExploration has the following parameter(s):
s: the string as received on Earth 
Input Format
There is one line of input: a single string, 
Note: As the original message is just SOS repeated times, 
's length will be a multiple of 
 */

import java.util.*;

import com.BasicProgramming.Runners.AbstractHacker;

public class HackerRankSolution1 extends AbstractHacker {
	public HackerRankSolution1() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Please enter the String");
		String s = sc.nextLine();
		int totalCount = marsExploration(s);
		System.out.println(totalCount);
	}

	// Complete the marsExploration function below.
	static int marsExploration(String s) {

		int count = 0;
		String iniString = "";
		int sLength = s.length();
		if (s.length() % 3 == 0) {
			for (int k = 0; k < sLength; k++) {
				iniString = s.substring(k, (k + 3));
				System.out.println(iniString);
				break;
			}
			for (int i = 3; i <= sLength - 1;) {
				String sub = s.substring(i, (i + 3));
				System.out.println(sub);
				if (!sub.equals(iniString)) {
					for (int j = 0; j <= 2; j++) {
						if (sub.charAt(j) != iniString.charAt(j)) {
							count = count + 1;
						}
					}
				}
				i = i + sub.length();
			}

		} else {
			return count;
		}
		return count;
	}

}