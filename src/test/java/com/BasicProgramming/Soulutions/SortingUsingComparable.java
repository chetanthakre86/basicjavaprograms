package com.BasicProgramming.Soulutions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.BasicProgramming.Runners.AbstractSortingCollection;

/**
 * @author Sanyu
 *
 */
public class SortingUsingComparable extends AbstractSortingCollection {
	List<Agents> agentList = new ArrayList<Agents>();

	public SortingUsingComparable() {
		calligToComparable();
	}

	class Agents implements Comparable<Agents> {
		String agentName;
		int salary;

		public Agents(String empName, int salary) {
			this.agentName = empName;
			this.salary = salary;

		}

		public String getEmpName() {
			return agentName;
		}

		public void setEmpName(String agentName) {
			this.agentName = agentName;
		}

		public int getSalary() {
			return salary;
		}

		public void setSalary(int salary) {
			this.salary = salary;
		}

		public int compareTo(Agents arg0) {

			return (this.getSalary() < arg0.getSalary() ? -1 : (this.getSalary() == arg0.getSalary() ? 0 : 1));
		}

		public String toString() {
			return "[agentName=" + this.agentName + ",salary=" + this.salary + "]";

		}

	}

	public void calligToComparable() {
		Agents agents = new Agents("Chetan", 50000);
		Agents agents1 = new Agents("Sanyu", 4999);
		Agents agents2 = new Agents("Thakre", 39999);
		agentList.add(agents);
		agentList.add(agents1);
		agentList.add(agents2);
		for (int i = 0; i < agentList.size(); i++) {
			System.out.println(agentList.get(i));

		}
		System.out.println("###########Sorted Order###############");
		Collections.sort(agentList);
		agentList.forEach(System.out::println);

	}

}
