package com.BasicProgramming.Soulutions;

import java.util.Deque;
import java.util.LinkedList;

import com.BasicProgramming.Runners.AbstractHacker;

public class HackerRankSolution9 extends AbstractHacker {
	Deque<Character> stackQ = new LinkedList<Character>();

	public HackerRankSolution9() {

		boolean result = balancedBracket();
		if (result) {
			System.out.println("Valid Paranthesis");
		} else {
			System.out.println("In-Valid Parenthesis");
		}

	}

	private boolean balancedBracket() {
		String brackets = "([[ff]{{[(())]}}])";
		for (char ch : brackets.toCharArray()) {
			if (ch == '{' || ch == '[' || ch == '(') {
				stackQ.addFirst(ch);
			}
			if (!stackQ.isEmpty() && ((stackQ.peekFirst() == '{' && ch == '}'))
					|| (stackQ.peekFirst() == '[' && ch == ']') || (stackQ.peekFirst() == '(' && ch == ')')) {
				stackQ.removeFirst();
			}
		}
		return stackQ.isEmpty();

	}
}