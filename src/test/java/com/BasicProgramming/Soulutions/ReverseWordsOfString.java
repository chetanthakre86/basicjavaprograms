package com.BasicProgramming.Soulutions;

import java.util.Scanner;

class ReverseSomeLetr {

	public static Scanner sc = new Scanner(System.in);

	ReverseSomeLetr() {

		System.out.println("Please enter the input string :");
		String str = sc.nextLine();
		reverseLetr(str);
		reverseString(str);
	}

	protected void reverseLetr(String str) {

		String[] splitString = str.split(" ");

		for (int i = 0; i < splitString.length; i++) {

			String word = splitString[i];
			String reverseWord = "";
			for (int k = word.length() - 1; k >= 0; k--) {
				reverseWord = reverseWord + word.charAt(k);
			}
			System.out.print(reverseWord + " ");

		}
	}

	protected void reverseString(String str) {
		int length = str.length();
		String reverse = "";
		for (int j = length - 1; j >= 0; j--) {
			reverse = reverse + str.charAt(j);

		}

		System.out.print(reverse);
	}
}

public class ReverseWordsOfString {

	public static void main(String[] args) {

		ReverseSomeLetr rvsLetter = new ReverseSomeLetr();
	}

}
