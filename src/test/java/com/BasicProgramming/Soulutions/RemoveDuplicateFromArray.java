package com.BasicProgramming.Soulutions;

import java.util.HashSet;
import java.util.InputMismatchException;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;

class ArrayElements {

	public int removeDuplicates(int[] arr, int length) {
		if (length == 0 || length == 1) {
			return length;
		}
		int temp[] = new int[length];
		for (int i = 0; i < length - 1; i++) {
			for (int l = i + 1; l <= length - 1; l++) {
				if (arr[i] == arr[l]) {

					arr[l] = 0;
				}
			}

		}

		return length;
	}

	public void removeDuplicateUsingCollection() {
		Set<Integer> set = new HashSet<Integer>();
		int a[] = { 11, 2, 3, 6, 5, 8, 5, 6 };
		for (int i : a) {
			set.add(i);
		}
		Iterator itr = set.iterator();
		while (itr.hasNext()) {
			System.out.print(itr.next() + " ");
		}
	}
}

public class RemoveDuplicateFromArray {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Please enter the size of the array");
		int n = sc.nextInt();
		int arr[] = new int[n];
		System.out.println("Please enter the element of the array");

		try {
			for (int i = 0; i < arr.length; i++) {
				arr[i] = sc.nextInt();
			}
		} catch (InputMismatchException e) {

			System.out.println("Please enter numbers only");
			return;
		}

		for (int k = 0; k < n; k++) {
			System.out.print(+arr[k] + " ");
		}
		ArrayElements arrayElements = new ArrayElements();
		arrayElements.removeDuplicates(arr, n);
		System.out.println("New Array is :");
		for (int i = 0; i < n; i++) {

			if (arr[i] != 0) {
				System.out.print(arr[i] + " ");
			}

		}

	}
}
