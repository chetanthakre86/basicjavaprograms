package com.BasicProgramming.Soulutions;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import com.BasicProgramming.Runners.AbstractStringProgram;

/**
 * @author Sanyu
 *
 */
public class DuplicateWords extends AbstractStringProgram {
	Scanner sc = new Scanner(System.in);

	public DuplicateWords() {
		findDuplicateWords();
		charRepetation();
	}

	public void findDuplicateWords() {
		System.out.println("Please enter the string : ");
		// String[] input1 = sc.nextLine().split(" ");
		Map<Integer, String> map = new LinkedHashMap<Integer, String>();
		String[] input1 = { "Welcome", "Chetan", "Welcome", "To", "MasterCard", "To" };

		for (int i = 0; i < input1.length; i++) {
			for (int j = i + 1; j < input1.length; j++) {
				if (input1[i].equalsIgnoreCase(input1[j])) {
					map.put(i, input1[i].toString());
					map.put(j, input1[i].toString());
				}
			}
		}
		Set<Map.Entry<Integer, String>> setEntry = map.entrySet();
		for (Map.Entry<Integer, String> mp : setEntry) {

			System.out.println(mp.getValue() + ":" + mp.getKey());

		}

	}
	
	public void charRepetation() {
		String s="ashutoshashutoshz";
		Map<Character,Integer>map=new HashMap<Character,Integer>();
		char []sChar=s.toCharArray();
		for(Character ch:sChar) {
			if(map.containsKey(ch)) {
				map.put(ch, map.get(ch)+1);
			}else {
				map.put(ch, 1);
			}
		}
		System.out.println("######## Finding the repeatating Characters############");
		Set<Map.Entry<Character,Integer>> entryMap=map.entrySet();
		for(Map.Entry<Character, Integer> setMap:entryMap) {
			System.out.println(setMap.getKey() +" : "+ setMap.getValue());
		}
	}

}
