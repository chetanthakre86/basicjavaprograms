package com.BasicProgramming.Soulutions;

import com.BasicProgramming.Runners.StarAndNumericPattern;

/**
 * @author Sanyu
 *
 */
public class CentarNumericPattern extends StarAndNumericPattern {

	public CentarNumericPattern() {
		printNumericPattern();
	}

	public void printNumericPattern() {
		int i, j;
		for (i = 0; i <= noOfRows; i++) {

			for (j = noOfRows; j > i; j--) {
				System.out.print(" ");
			}
			for (int k = 0; k <= i; k++) {

				System.out.print(k + 1 + " ");
			}
			System.out.println();
		}

	}
}
