package com.BasicProgramming.Soulutions;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
@Retention(RetentionPolicy.RUNTIME)
public @interface RetryCountIffailed {
	int value() default 0;
}
