package com.BasicProgramming.Soulutions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.BasicProgramming.Runners.AbstractSortingCollection;

public class SortingUisngMethodReference extends AbstractSortingCollection {
	List<SoftDrinks> list = new ArrayList<SoftDrinks>();

	public SortingUisngMethodReference() {

	}

}

class SoftDrinks {
	String drinkDescription;
	String drinkName;
	int drinkQuantity;

	SoftDrinks(String drinkDescription, String drinkName, int drinkQuantity) {
		this.drinkDescription = drinkDescription;
		this.drinkName = drinkName;
		this.drinkQuantity = drinkQuantity;
	}

	public String getdrinkDescription() {
		return drinkDescription;
	}

	public void setdrinkDescription(String drinDescription) {
		this.drinkDescription = drinkDescription;
	}

	public String getDrinkName() {
		return drinkName;
	}

	public void setDrinkName(String drinkName) {
		this.drinkName = drinkName;
	}

	public int getDrinkQuantity() {
		return drinkQuantity;
	}

	public void setDrinkQuantity(int drinkQuantity) {
		this.drinkQuantity = drinkQuantity;
	}

	public int compareTo(SoftDrinks softD) {

		return (this.getDrinkQuantity() < softD.getDrinkQuantity() ? -1
				: (this.getDrinkQuantity() == softD.getDrinkQuantity() ? 0 : 1));
	}

}