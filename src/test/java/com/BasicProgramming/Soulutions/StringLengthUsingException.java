package com.BasicProgramming.Soulutions;

import java.util.Scanner;

import com.BasicProgramming.Runners.AbstractStringProgram;

public class StringLengthUsingException extends AbstractStringProgram {
	Scanner sc = new Scanner(System.in);

	public StringLengthUsingException() {
		System.out.println("Please Enter the String");
		String input = sc.nextLine();
		int len = lengthUsingException(input);
		System.out.println("Length of the String is: " + len);
	}

	private int lengthUsingException(String input) {
		int count = 0;
		while (1 > 0) {
			try {
				char c = input.charAt(count);
				count++;
			} catch (Exception e) {

				return count;
			}
		}
	}
}
