package com.BasicProgramming.Soulutions;

import com.BasicProgramming.Runners.StarAndNumericPattern;

/**
 * @author Sanyu
 *
 */
public class CenterStarPattern extends StarAndNumericPattern {

	public CenterStarPattern() {
		centerEvenStarPattern();
		centerOddStarPattern();
	}

	public void centerEvenStarPattern() {
		System.out.println("##################################");
		int i, j;
		for (i = 0; i <= noOfRows; i++) {

			for (j = noOfRows; j > i; j--) {
				System.out.print(" ");
			}
			for (int k = 0; k <= i; k++) {
				System.out.print("* ");
			}
			System.out.println();
		}
	}

	public void centerOddStarPattern() {
		System.out.println("##################################");
		int i, j;
		for (i = 0; i <= noOfRows; i++) {

			for (j = noOfRows; j > i; j--) {
				System.out.print(" ");
			}
			for (int k = 0; k <= 2 * i; k++) {
				System.out.print("* ");
			}
			System.out.println();
		}
	}

}
