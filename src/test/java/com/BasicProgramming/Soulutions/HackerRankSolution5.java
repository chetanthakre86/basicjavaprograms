package com.BasicProgramming.Soulutions;

import java.io.*;
import java.util.*;

import com.BasicProgramming.Runners.AbstractHacker;

/**
 * PENDING
 * 
 * @author Chetan A string is said to be a special string if either of two
 *         conditions is met:
 * 
 *         All of the characters are the same, e.g. aaa. All characters except
 *         the middle one are the same, e.g. aadaa. A special substring is any
 *         substring of a string which meets one of those criteria. Given a
 *         string, determine how many special substrings can be formed from it.
 */

public class HackerRankSolution5 extends AbstractHacker {
	private static final Scanner scanner = new Scanner(System.in);

	public HackerRankSolution5() {
		String s = scanner.nextLine();
		int n = s.length();
		long result = substrCount(n, s);
		System.out.println(result);

	}

	// Complete the substrCount function below.

	static long substrCount(int n, String s) {
		int a = n;
		for (int i = 0; i < n - 1; i++) {
			for (int j = i + 1; j < n; j++) {
				String sub = "";
				try {

					sub = sub + s.charAt(i) + s.charAt(j);
				} catch (StringIndexOutOfBoundsException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				String rev = "";

				for (int k = sub.length() - 1; k >= 0; k--) {

					rev = rev + sub.charAt(k);

				}
				if (sub.equalsIgnoreCase(rev)) {
					System.out.println(sub + " : " + rev);
					a++;
				}

			}
		}
		return a;

	}

}