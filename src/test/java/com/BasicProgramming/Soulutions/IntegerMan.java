package com.BasicProgramming.Soulutions;
import java.util.Scanner;

/**
 * @author Sanyu
 *
 */
public class IntegerMan extends AbstractIntegerProgram {
	Scanner sc = new Scanner(System.in);

	IntegerMan() {
		reverseNumber();
		reverseNumberUsingFor();
	}

	public void reverseNumber() {
		System.out.println("Please Enter the no");
		int num = sc.nextInt();
		int revNum = 0;
		while (num != 0) {
			revNum = (revNum * 10) + (num % 10);
			num /= 10;
		}
		System.out.println("Reverse no using while loop is : " + revNum);
	}

	public void reverseNumberUsingFor() {
		System.out.println("Please Enter the no");
		int num = sc.nextInt();
		int revNum = 0;
		for (; num != 0; num /= 10) {
			revNum = (revNum * 10) + (num % 10);

		}
		System.out.println("Reverse no using For loop is : " + revNum);
	}

}
