package com.BasicProgramming.Soulutions;

import com.BasicProgramming.Runners.StarAndNumericPattern;

/**
 * @author Chetan
 *
 */
public class RightNumericPattern extends StarAndNumericPattern {

	public RightNumericPattern() {
		printRightNumericPattern();
	}

	public void printRightNumericPattern() {
		int i, j;
		for (i = 0; i <= noOfRows; i++) {

			for (j = noOfRows; j > i; j--) {
				System.out.print(" ");
			}
			for (int k = 0; k <= i; k++) {

				System.out.print(k + 1 + " ");
			}
			System.out.println();
		}

	}

}
