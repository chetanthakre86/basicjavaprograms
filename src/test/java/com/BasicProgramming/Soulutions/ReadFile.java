package com.BasicProgramming.Soulutions;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

public class ReadFile {
	public static File file;
	static int row;
	static int column;

	public static void main(String[] args) throws Exception {
		ReadExcel readExcel = new ReadExcel();
		ReadExcel.extensionOfFile(row, column);
	}
}

class ReadExcel {
	static String fileExtension;
	public static Workbook workbook;
	public static XSSFWorkbook xslxWorkbook;
	public static HSSFWorkbook xlsWorkbook;
	public static File file;
	public static Sheet sheet;
	public static Row row;
	public static Cell cell;
	private static final String fileName = "C:\\Users\\A697651\\MyWorkspace\\ProgrammingTest\\SuiteOne.xls";

	public static void extensionOfFile(int row, int column) throws IOException {
		// To get value of file extension ... other way needs to be find out how
		// to transfer variable value from one method to other
		try {
			File file = new File(fileName);
			FileInputStream fis = new FileInputStream(file);
			fileExtension = extensionOfFile(file);
			if (fileExtension.equals("xls")) {
				Workbook workbook = new HSSFWorkbook(fis);
				System.out.println("file having extension .xls will be opened");
				readExcel(workbook);

			} else {
				Workbook workbook = new XSSFWorkbook(fis);
				System.out.println("file having extension .xlsx will be opened");
				readExcel(workbook);
			}
			fis.close();
		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	// To read excel file
	public static String readExcel(Workbook workbook) {
		String cellData = null;
		Sheet sheet = workbook.getSheet("SuiteOneCaseOne");
		Iterator<Row> rowIterator = sheet.rowIterator();
		while (rowIterator.hasNext()) {
			row = rowIterator.next();
			Iterator<Cell> cellIterator = row.cellIterator();
			while (cellIterator.hasNext()) {
				cell = cellIterator.next();
				cellData = cell.getStringCellValue();
				return cellData;
			}

		}
		return "";

	}

	// To check extension of a file if make private wont be visible in other class
	public static String extensionOfFile(File file) throws Exception {
		int startExtension;
		// To check start of extension
		startExtension = file.toString().indexOf(".");
		// To extract extension
		fileExtension = file.toString().substring(startExtension + 1, file.toString().length());
		// fileExtension1=(String) file.toString().subSequence(startExtension,
		// file.toString().length()); another way to get extension of a file
		System.out.println(fileExtension);

		/*
		 * OR To extract extension System.out.println(file.getName()); boolean
		 * ext=file.getName().endsWith(".xls"); System.out.println(ext);
		 */

		return fileExtension;
	}

	public String[] sendToRunutility(String sheetName, String rowName, String columnName) {
		int sheetIndex = workbook.getSheetIndex(sheetName);

		if (sheetIndex == -1) {
			return null;
		} else {
			int rowin = retrivesNoRows(sheetName);
			int colIn = retrivesNoColumns(sheetName);
			int rownumb = -1;
			int colunumb = -1;

			Row row1 = sheet.getRow(0);
			String[] data = new String[rowin - 1];
			for (int i = 0; i < colIn; i++) {
				if (row1.getCell(i).getStringCellValue().equals(columnName.trim())) {
					colunumb = i;
				}
			}
			return data;
		}
	}

	// To get no of rows
	public int retrivesNoRows(String sheetName) {
		int sheetIndex = workbook.getSheetIndex(sheetName);
		if (sheetIndex == -1) {
			return 0;
		} else {
			workbook.getSheetAt(sheetIndex);
			int noOfRows = sheet.getLastRowNum() + 1;
			return noOfRows;
		}
	}

	// To get no of columns
	public int retrivesNoColumns(String sheetName) {
		int sheetIndex = workbook.getSheetIndex(sheetName);
		if (sheetIndex == -1) {
			return 0;
		} else {
			workbook.getSheetAt(sheetIndex);
			int noOfColumn = sheet.getRow(0).getLastCellNum();
			return noOfColumn;
		}
	}
}