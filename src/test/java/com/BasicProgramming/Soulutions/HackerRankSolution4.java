package com.BasicProgramming.Soulutions;

import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

import com.BasicProgramming.Runners.AbstractHacker;

/*Completed
 * To find no of palindrome in a String
 * Also print the shortest and longest palindrome of a string
 * I have used TreeMap and its methods firstEntry() And lastEntry() 
	*/

public class HackerRankSolution4 extends AbstractHacker {
	public static Scanner sc = new Scanner(System.in);

	public HackerRankSolution4() {
		System.out.println("Please enter the string : ");
		String input = sc.nextLine();
		findAllPalindromeInString(input);
	}

	public void findAllPalindromeInString(String input) {
		Map<Integer, String> mapPalindrome = new TreeMap<Integer, String>();
		for (int i = 0; i < input.length(); i++) {
			for (int j = i + 1; j <= input.length(); j++) {
				if (isPalindrome(input.substring(i, j))) {
					String mapString = input.substring(i, j);
					int len = mapString.length();
					mapPalindrome.put(len, mapString);

				}
			}
		}
		System.out.println(mapPalindrome);
		System.out.println("Please enter which palindrome choice: Shortest or Longets");
		String choice = sc.nextLine();
		if (choice.equalsIgnoreCase("shortest")) {
			System.out.println("Shortest Palindrome is : " + ((TreeMap<Integer, String>) mapPalindrome).firstEntry());
		} else {
			System.out.println(((TreeMap<Integer, String>) mapPalindrome).lastEntry());
		}

	}

	private boolean isPalindrome(String input) {
		StringBuilder sb = new StringBuilder(input);
		StringBuilder revString = sb.reverse();
		return (revString.toString().contentEquals(input));

	}

}
