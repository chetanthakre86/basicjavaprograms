package com.BasicProgramming.Soulutions;

import java.util.Arrays;

import com.BasicProgramming.Runners.ArrayManipulation;

/*Multidimensional Arrays can be defined in simple words as array of arrays. Data in multidimensional arrays are 
 * stored in tabular form (in row major order).
 * data_type[1st dimension][2nd dimension][]..[Nth dimension] array_name = new data_type[size1][size2]�.[sizeN];
 * */
/*public class MultiDimnsionalArraySorting {

	public static void main(String[] args) {
		MultiDimensionalArray mulArr = new MultiDimensionalArray();
		mulArr.findLargestNo();
	}

}*/

public class MultiDimensionalArray extends ArrayManipulation {
	public MultiDimensionalArray(){
		findLargestNo();
	}

	public void findLargestNo() {
		double array[][] = { { 23, 67, 89, 1 }, { 9, 2, 0, 5, 10 }, { 19, 21, 56, 99, 0 } };
		int lenArray = array.length;
		double result[] = new double[lenArray];
		int k = -1;
		for (int i = 0; i < lenArray; i++) {
			double temp = 0;
			int lenFirstArray = array[i].length;
			//Arrays.sort(array[i]);
			for (int j = 0; j < lenFirstArray; j++) {
				if (array[i][j] > temp) {
					temp = array[i][j];
				}

			}
			for (k = k + 1; k < lenArray; k++) {
				result[k] = temp;
				break;
			}

		}
		System.out.println("Soretd array is :");
		for (int z = 0; z < result.length; z++) {
			System.out.print(result[z] + ",");
		}

	}
}