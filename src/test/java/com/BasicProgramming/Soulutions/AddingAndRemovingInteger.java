package com.BasicProgramming.Soulutions;

import java.util.Scanner;

import com.BasicProgramming.Runners.ArrayManipulation;

/**
 * @author Sanyu
 *
 */
public class AddingAndRemovingInteger extends ArrayManipulation {
	Scanner sc = new Scanner(System.in);

	public AddingAndRemovingInteger() {
		// inputDetails();
		addingAnElement();

	}

	public void inputDetails() {

	}

	public void addingAnElement() {
		// To take array input
		System.out.println("Please enter length of an array");
		int n = sc.nextInt();
		int a[] = new int[n];
		System.out.println("Please enter required array elements");

		try {
			for (int i = 0; i < n; i++) {
				try {
					a[i] = sc.nextInt();
				} catch (ArrayIndexOutOfBoundsException ex) {
					ex.printStackTrace();
				}
			}
		} catch (ArrayIndexOutOfBoundsException e) {

			e.printStackTrace();
		}
		int length = a.length;
		System.out.println("Please enter the position and element you want to insert");
		int pos = sc.nextInt();
		int element = sc.nextInt();
		for (int i = (length - 1); i >= (pos - 1); i--) {
			try {
				a[i + 1] = a[i];
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		a[pos - 1] = element;
		try {
			for (int j = 0; j < length-1; j++) {
				System.out.print(a[j] + ",");
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
