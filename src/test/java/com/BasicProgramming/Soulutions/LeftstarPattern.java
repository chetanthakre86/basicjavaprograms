package com.BasicProgramming.Soulutions;

import com.BasicProgramming.Runners.StarAndNumericPattern;

public class LeftstarPattern extends StarAndNumericPattern {

	public LeftstarPattern() {
		printPattern();
		printAllStarPattern();
		printWholeLeftPattern();
	}

	public void printPattern() {

		for (int i = 0; i <= noOfRows; i++) {
			for (int j = 0; j <= i; j++) {
				System.out.print("* ");
			}
			System.out.println();
		}
	}

	public void printAllStarPattern() {
		System.out.println("#######################");
		for (int i = 0; i <= noOfRows; i++) {
			for (int j = 0; j <= noOfRows - i; j++) {
				System.out.print("* ");
			}
			System.out.println();
		}
	}

	public void printWholeLeftPattern() {
		System.out.println("#######################");
		for (int i = 0; i <= noOfRows; i++) {
			for (int j = 0; j <= i; j++) {
				System.out.print("* ");
			}
			System.out.println();
		}

		for (int i = 1; i <= noOfRows; i++) {
			for (int j = 0; j <= noOfRows - i; j++) {
				System.out.print("* ");
			}
			System.out.println();
		}

	}
}