package com.BasicProgramming.Soulutions;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Scanner;
import java.util.Set;

import com.BasicProgramming.Runners.AbstractStringProgram;

/**
 * @author Sanyu
 *
 */
public class AddRemoveStringElements extends AbstractStringProgram {
	String input;

	public AddRemoveStringElements() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Please enter the string");
		input = sc.nextLine();

		removeElementUsingSet();
		removeDuplicatesElementFromString();

	}

	public void removeDuplicatesElementFromString() {
		// Convert string to char array
		char[] charArray = input.toCharArray();
		int len = charArray.length;
		int index = 0;
		for (int i = 0; i < len; i++) {
			int j;
			for (j = 0; j < i; j++) {
				// Check character is present or not
				if (charArray[i] == charArray[j]) {
					break;
				}
			}
			// character is not present add it to the index
			if (j == i) {
				charArray[index++] = charArray[i];

			}
		}
		String result = String.valueOf(Arrays.copyOf(charArray, index));
		System.out.println();
		System.out.print("Unique String Without Colloction is :" + result);
	}

	public void removeElementUsingSet() {
		char chInput[] = input.toCharArray();
		// Set set = new LinkedHashSet();
		Set<Character> set = new LinkedHashSet<Character>();
		for (Character ch : chInput) {
			set.add(ch);
		}
		System.out.println("New String is : ");
		Iterator itr = set.iterator();
		while (itr.hasNext()) {
			System.out.print(itr.next());
		}

	}
}
