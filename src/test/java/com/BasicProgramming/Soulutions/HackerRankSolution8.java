/*Problem Statement
 * Example
 * Input				Output
 * abcd				A-Bb-Ccc-Dddd
 * RTGS				R-Tt-Ggg-Ssss
 *NefT				N-Ee-Fff-Tttt
 * */
package com.BasicProgramming.Soulutions;

import java.util.Scanner;

import com.BasicProgramming.Runners.AbstractHacker;

public class HackerRankSolution8 extends AbstractHacker {
	Scanner sc = new Scanner(System.in);

	public HackerRankSolution8() {
		printCharacterPattern();
	}

	protected void printCharacterPattern() {
		System.out.println("Please enter the string");
		String s = sc.nextLine();
		String[] trimString = s.split(" ");
		int tempLength = 0;
		int length = trimString.length;
		String finalString = "";
		for (int i = 0; i < length; i++) {
			String patString = trimString[i];
			int conLength = tempLength + patString.length();
			for (int k = 0; k < patString.length(); k++) {
				char upperChar = Character.toUpperCase(patString.charAt(k));
				finalString = finalString + Character.toString(upperChar);
				int z = k + tempLength;
				while (z > 0) {
					char lowerCase = Character.toLowerCase(patString.charAt(k));
					finalString = finalString + Character.toString(lowerCase);
					z--;
				}
				finalString = finalString + "-";
			}
			tempLength = conLength;

		}
		System.out.println(finalString.substring(0, finalString.length() - 1));
		sc.close();
	}

}
