package com.BasicProgramming.Soulutions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.BasicProgramming.Runners.AbstractSortingCollection;

public class SortingUsingLambdaExpression extends AbstractSortingCollection {
	List<Students> list = new ArrayList<Students>();

	public SortingUsingLambdaExpression() {
		setAndSortStudentDetails();
	}

	public void setAndSortStudentDetails() {
		list.add(new Students(120, "Chetan", "Pune"));
		list.add(new Students(101, "Sanyu", "Ngp"));
		list.add(new Students(501, "Thakre", "Mumbai"));
		sortByLambda();
	}

	public void sortByLambda() {
		Collections.sort(list,(s1,s2)->s1.address.compareTo(s2.address));
		System.out.println("Sort By address Using Lambda Expression");
		list.forEach(System.out::println);
	}
	
	
}
