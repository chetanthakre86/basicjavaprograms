package com.BasicProgramming.Soulutions;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

import com.BasicProgramming.Runners.AbstractHacker;

/**
 * Pending
 * 
 * @author Chetan You are given a string containing characters and only. Your
 *         task is to change it into a string such that there are no matching
 *         adjacent characters. To do this, you are allowed to delete zero or
 *         more characters in the string. Your task is to find the minimum
 *         number of required deletions. For example s=AABAAB, given the string
 *         , remove an A at positions 0 and 3 to make s=ABAB in 2 deletions.
 */

public class HackerRankSolution7 extends AbstractHacker {
	private static final Scanner scanner = new Scanner(System.in);

	public HackerRankSolution7() {
		int q = scanner.nextInt();
		scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

		for (int qItr = 0; qItr < q; qItr++) {
			String s = scanner.nextLine();

			int result = alternatingCharacters(s);
			System.out.println(result);

			/*
			 * bufferedWriter.write(String.valueOf(result));
			 * bufferedWriter.newLine();
			 */
		}

		// bufferedWriter.close();

		scanner.close();
	}

	// Complete the alternatingCharacters function below.
	static int alternatingCharacters(String s) {
		int count = 0;
		char a[] = s.toCharArray();
		int len = a.length;
		for (int i = 0; i < len - 1; i++) {
			try {
				if (a[i] == a[i + 1]) {
					count++;
					a[i] = 0;
					// System.out.println(a);
				}
			} catch (ArrayIndexOutOfBoundsException e) {

				e.printStackTrace();
			}

		}
		System.out.println(a);
		return count;

	}

}
