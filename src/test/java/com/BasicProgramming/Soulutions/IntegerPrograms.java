package com.BasicProgramming.Soulutions;
import java.util.Scanner;

/**
 * @author Sanyu
 */
public class IntegerPrograms {
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		System.out.println("Please enter the class name");
		String classType = sc.nextLine();
		IntegerOperation inter = new IntegerOperation();
		inter.getInstance(classType);
	}

}

abstract class AbstractIntegerProgram {

}

class IntegerOperation {
	public AbstractIntegerProgram getInstance(String classType) {

		if (classType == null) {
			return null;
		} else if (classType.equalsIgnoreCase("Integer")) {
			return new IntegerMan();
		}
		return null;

	}
}