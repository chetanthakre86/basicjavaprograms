package com.BasicProgramming.Soulutions;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;

import com.BasicProgramming.Runners.AbstractStringProgram;

/**
 * @author Sanyu
 *
 */
public class ReverseString extends AbstractStringProgram {
	String input;
	private Set<Entry<Integer, String>> String;

	public ReverseString() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Please enter the string");
		input = sc.nextLine();

		reverseEachWordAtPosition();
		reverseMiddleWord();
		findAllSubstring(input);
	}

	private void reverseEachWordAtPosition() {

		String[] splitInput = input.split(" ");
		for (int i = 0; i < splitInput.length; i++) {
			String word = splitInput[i];
			String reverseWord = "";
			for (int j = word.toCharArray().length - 1; j >= 0; j--) {
				reverseWord = reverseWord + word.charAt(j);
			}
			System.out.print(reverseWord + " ");
		}
		System.out.println("####################");
	}

	private void reverseMiddleWord() {
		String[] splitInput = input.split(" ");
		String reverseWord = "";
		for (int i = 1; i < splitInput.length - 1; i++) {
			String word = splitInput[i];
			for (int j = word.toCharArray().length - 1; j >= 0; j--) {
				reverseWord = reverseWord + word.charAt(j);
			}

		}
		System.out.println("########## Reversing Middle words only ##################");
		System.out.println(splitInput[0] + " " + reverseWord + " " + splitInput[splitInput.length - 1]);
	}

	private void findAllSubstring(String input) {
		System.out.println("###########################");
		Map<Integer, String> map = new TreeMap();
		String[] splitInput = input.split(" ");
		for (int i = 0; i < input.length(); i++) {
			for (int j = i + 1; j < input.length(); j++) {
				if (isPalindrome(input.substring(i, j))) {
					String mapString = input.substring(i, j);
					int len = mapString.length();
					map.put(len, mapString);
				}
			}
		}
		Set<Map.Entry<Integer, String>> setView = map.entrySet();
		for (Map.Entry<Integer, String> mp : setView) {
			System.out.println("All Substrings are: " + mp.getValue() + ":" + mp.getValue());
		}

	}

	private boolean isPalindrome(String substr) {
		StringBuilder sb = new StringBuilder(substr);
		StringBuilder reve = sb.reverse();
		return (reve.equals(substr));
	}

}
