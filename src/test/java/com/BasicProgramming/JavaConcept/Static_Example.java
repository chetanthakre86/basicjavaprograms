package com.BasicProgramming.JavaConcept;

import com.BasicProgramming.JavaConcept.OuterClasss.InnerClassStatic;

public class Static_Example {

	public static void main(String[] args) {
		InnerClassStatic stat = new InnerClassStatic();
		stat.innerClassNonStaticMethod(); // Call by using object creation of a
											// static class
		InnerClassStatic.innerClassStaticMethod();

	}

}

/*
 * Outer class can not be "static". Only inner class can be static. only Static
 * method of outer class can be called inside the inner static class(Only inside
 * the method). Inner static class is visible outside the outer class for that
 * we need to import class using outer class reference. (Example
 * OuterClasss.InnerClassStatic) We can extends Inner static class to other
 * inner class
 */
class OuterClasss {
	static int a = 10;

	static void outerClassStaticMethod() {
		System.out.println("Inside the Outer Class static method");
	}

	void outerClassNonStaticMethod() {
		System.out.println("Inside the Outer Class non-static method");
	}

	static class InnerClassStatic {
		InnerClassStatic() {
			// innerClassMethod1();
		}

		static void innerClassStaticMethod() {
			System.out.println("Inside the Inner Class static method");
			a = 20;
			System.out.println("Outer class static variable :" + a);
			outerClassStaticMethod();

		}

		void innerClassNonStaticMethod() {
			System.out.println("Inside the Inner Class non static method");
			a = 20;
			System.out.println("Outer class static variable :" + a);

		}

		private void innerClassPrivateMethod() {
			System.out.println("Inside the Inner Class non static method");
			a = 20;
			System.out.println("Outer class static variable :" + a);

		}

	}
}