/*private method can not be override.We can write same private  method inside child class,
 * compiler wont through compile time error but at runtime, We can not call that method
 *  by runtime polymorphism concept.*/

package com.BasicProgramming.JavaConcept;

public class DynamicMethodDispatch {

	public static void main(String[] args) {
		Base base = new ChildBase();
		//ChildBase child=((ChildBase)Base).hide(); vut need to change return type of method
	}

}

class Base {
	Base(){
		show();
		display(20.20);
	}
	void display(int a) {
		System.out.println("This is parent class");
	}

	void show() {
		System.out.println("This show method is of parent class");
	}

	private void display(double b) {
		System.out.println("This is parent private method");
	}

}

class ChildBase extends Base {
	ChildBase(){
		display(20.20);
	}
	void hide() {
		System.out.println("Inside hide method of child class");

	}

	void show() {
		System.out.println("This is show method of child class");

	}

	private void display(double b) {
		System.out.println("this is child class private method");
	}
}