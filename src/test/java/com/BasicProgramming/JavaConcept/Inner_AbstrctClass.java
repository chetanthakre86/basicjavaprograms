package com.BasicProgramming.JavaConcept;

public class Inner_AbstrctClass {

	public static void main(String[] args) {
		D d = new D();

	}

}

class D {
	abstract class B {
		abstract void show();
	}

	class C extends B {
		@Override
		void show() {
			System.out.println("Inside override class");
		}
	}

	B b = new C();

}