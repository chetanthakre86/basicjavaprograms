/*The reason why we get compile time error in the above scenario is, here the method arguments
 *  Integer and String both are not primitive data types in Java. That means they accept null values.
 *   When we pass a null value to the method1 the compiler gets confused which method it has to 
 *   select, as both are accepting the null*/
package com.BasicProgramming.JavaConcept;

public class MostSpecific_OverloadedMethod {

	public static void main(String[] args) {
		MostSpecificCalling obj = new MostSpecificCalling();
		Object ob = null;
		obj.test(ob); // will call method having object data type
		obj.test(null);// will call method having String data type because most
						// specific calling
		/*
		 * int []arr=null; obj.test(arr);
		 */
	}
}

class MostSpecificCalling {
	public void test(String obj1) {
		System.out.println("Calling method having String data type");
	}

	public void test(Object obj1) {
		System.out.println("Calling method having Object data type");

	}
	/*
	 * public void test(int[] intArr) { System.out.println("int array"); }
	 * 
	 * public void test(char[] charArr) { System.out.println("char array"); }
	 */

	/*
	 * public void test(char[] obj) { System.out.println("Char"); }
	 * 
	 * public void test(Integer obj) { System.out.println("Int"); }
	 */
	/*
	 * public void test(Integer obj1) {
	 * System.out.println("Calling method having Char data type"); }
	 */
	public void test(long lng) {
		System.out.println("Long");
	}

}