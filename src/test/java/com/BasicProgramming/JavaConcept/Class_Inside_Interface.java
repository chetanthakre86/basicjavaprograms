package com.BasicProgramming.JavaConcept;

/**
 * @author Chetan Thakre
 *
 */
interface Class_Inside_Interface {
	
		abstract class Inside_interface{
			 void display(){
				System.out.println("inside abstract class");
			}
			abstract void show();
		
		}
		 class A extends Inside_interface {

			@Override
			void show() {
				System.out.println("Inside extended method");
				
			}	
		}
		
		
}
