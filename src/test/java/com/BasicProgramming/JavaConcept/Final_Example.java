package com.BasicProgramming.JavaConcept;

public class Final_Example {

	public static void main(String[] args) {
		ExFinal ef = new ExFinal();
		ExFinal ef1 = new ExFinal(30);
		ExFinal fi = new FinalInherit();

	}

}

class ExFinal {
	final int a;

	ExFinal() {
		a = 10;
		System.out.println("Inside ExFinal not parameterised constructor: " + a);
	}

	ExFinal(int b) {
		this.a = b;
		System.out.println("Inside ExFinal parameterised constructor: " + a);
	}

	final void getint() {
		System.out.println(a);

	}

}

class FinalInherit extends ExFinal {
	/*
	 * final void getint(){ final method can not be inherited }
	 */

}