package com.BasicProgramming.JavaConcept;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileReade {

	public static void main(String[] args) throws IOException {
		ReadAndWriteFile.readInAFile();
	}

}

class ReadAndWriteFile {

	public static void readInAFile() throws IOException {
		FileInputStream isr = new FileInputStream(
				new File(System.getProperty("user.dir")) + File.separator + "Test.txt");
		char charac = (char) isr.read();
		System.out.println("This reads the single character " + charac);

		BufferedReader bufReader = new BufferedReader(
				new FileReader(System.getProperty("user.dir") + File.separator + "Test.txt"));
		BufferedWriter bufWrite = new BufferedWriter(
				new FileWriter(System.getProperty("user.dir") + File.separator + "Test1.txt"));
		// String readLine = bufReader.readLine();
		String readLine;
		while ((readLine = bufReader.readLine()) != null) {
			System.out.println(readLine);
			System.out.println("======================");
			bufWrite.write(readLine);
			bufWrite.newLine();

		}
		bufWrite.flush();
	}
}