package com.BasicProgramming.JavaConcept;

//@FunctionalInterface
public interface InterfaceFunctional {
	public void greet();

	/*
	 * If we add any other abstract method will give "<-Name of Interface> is
	 * not functional interface"
	 */
	// public void sayHello();
	int hashCode(); // object class method

	default void doIt() {
		System.out.println("Default Method inside parent Functional interface ");
	}

	static void getIT() {
		System.out.println("Static method inside parent Functional interface");
	}
}

@FunctionalInterface
interface Greed extends InterfaceFunctional {
	public void greet();

	default void doIt() {

		System.out.println("Default Method inside child Functional interface ");
	}
}