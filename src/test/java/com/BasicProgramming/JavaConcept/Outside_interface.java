package com.BasicProgramming.JavaConcept;

interface Outside_interface {

	interface Inner1 {
		void show();
	}

	interface Inner2 extends Inner1 {
		default void display() {
			System.out.println("Inside interface inner 2 ");
		}
	}

	class Inside_Interface implements Inner2 {

		@Override
		public void show() {
			System.out.println("Inside Inside interface class");

		}

		public void display() {
			System.out.println("calling class inside interface");
		}

	}

	public static void main(String[] args) {
		Inside_Interface insInterface = new Inside_Interface();
		insInterface.display();
		insInterface.show();
	}

}
