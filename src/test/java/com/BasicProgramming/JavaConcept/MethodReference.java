/*Four types of method references
1. Method reference to an instance method of an object � object::instanceMethod
2. Method reference to a static method of a class � Class::staticMethod
3. Method reference to an instance method of an arbitrary object of a particular type � Class::instanceMethod
4. Method reference to a constructor � Class::new
*/

package com.BasicProgramming.JavaConcept;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.plaf.synth.SynthSeparatorUI;

public class MethodReference {
	public void circle() {
		System.out.println("Draw Circle");
	}

	public static void main(String[] args) {
		// Method reference to an instance method of an object �
		// object::instanceMethod
		MethodReference refer = new MethodReference();
		InterfaceLambda lambda = refer::circle;
		lambda.draw();

		ReferMethod refer1 = new ReferMethod();
		lambda = refer1::triangle;
		lambda.draw();

		// Method reference to a static method of a class � Class::staticMethod
		MultiParameter multiParam = ReferMethod::multiply;
		int mult = multiParam.add(10, 20);
		System.out.println(mult);

		// Method reference to a constructor � Class::new
		ReferenceConstructor referenceConst = ConstructorReference::new;
		referenceConst.say("Hi There");

		InstanceMethodReference methodReference= new InstanceMethodReference();
		methodReference.fromClassType();
		methodReference.loopforEach();
	}

}

class ReferMethod {

	public void triangle() {
		System.out.println("Draw triangle");
	}

	public static int multiply(int a, int b) {
		return a * b;

	}
}

class InstanceMethodReference {
	List<String> list = Arrays.asList("how", "to", "do", "it", "in", "java");

	public void fromClassType() {
		System.out.println("###Instance Method Reference from Class Type");
		List<String> sortedList = list.stream().sorted(String::compareTo).collect(Collectors.toList());
		System.out.println(sortedList);
	}

	public void loopforEach() {
		System.out.println("###Printing Using For each loop ####");
		list.forEach(s->System.out.println(s)); //using for each 
		System.out.println("### Printing using For each ordered loop");
		list.stream().sorted().forEachOrdered(System.out::print); //we can also use syntax (forEachOrdered(name->System.out.println(name)))
	}

}

class ConstructorReference {
	ConstructorReference(String say) {
		System.out.println(say);
	}
}
