package com.BasicProgramming.JavaConcept;

public class MultiTimeCalling_OverloadedMethod {
	double overloadedMethod(double d) {
		return d *= d;
	}

	int overloadedMethod(int i) {
		return overloadedMethod(i *= i);
		// return i *= i;
	}

	float overloadedMethod(float f) {
		return overloadedMethod(f *= f);
	}

	public static void main(String[] args) {
		MultiTimeCalling_OverloadedMethod main = new MultiTimeCalling_OverloadedMethod();

		// System.out.println();
		main.overloadedMethod(100);
	}
}
