package com.BasicProgramming.JavaConcept;

public interface InterfaceExample {

	public static void main(String[] args) {
		Interfaceclass i = new Interfaceclass();
		i.childClassMethod();
		OneInterface one = new Interfaceclass();
		one.show();
	}
}

interface OneInterface {
	void show();

	static int b = 0;
	public static final int a = 5;
	/*
	 * void get(){ // non abstract method is not allowed
	 * 
	 * }
	 */
}

class Interfaceclass implements OneInterface {

	@Override
	public void show() {
		System.out.println("This is ovrridden method of an inteface inside child class");

	}

	public void childClassMethod() {
		System.out.println("Only child class method");
	}

}