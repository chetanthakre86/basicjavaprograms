package com.BasicProgramming.JavaConcept;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

public class Collection_Example {

	public static void main(String[] args) {
		List<String> li = new ArrayList<String>();
	li.add("XYZ");
	li.add("ABC");
	li.add(0, "A");
	Iterator it = li.iterator();
	System.out.println("===By using iterator we can print in List===");
	while(it.hasNext()){
		System.out.println(it.next());
	}
	System.out.println(li);
	System.out.println("===By using for-each loop we can print====");
	for(String s:li){
		System.out.println(s);
	}
	
	System.out.println("===List Iterator Example====");
	List li1 = new LinkedList();
	li1.add(20);
	li1.add("sss");
	li1.add(0, 50);
	System.out.println(li1);

	System.out.println("===Vector Example===");
	Vector v = new Vector();
	v.add(1);
	v.add("xx");
	Enumeration enm = v.elements();
	Iterator it4 = v.iterator();
	while(it4.hasNext()) {
		System.out.println(it4.next());
	}
	
	/*while(enm.hasMoreElements()){
		System.out.println(enm.nextElement());
	}*/
	System.out.println(v);
	}

}
