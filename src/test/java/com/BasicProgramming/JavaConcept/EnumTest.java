package com.BasicProgramming.JavaConcept;
/*
 * To declare own data type we have to use enum. Enum is collection of constants.
Enum arranges its constants in an array form.
By default object inside the enum are public static and final
Enum can have constructor and its by default private. 
Enum can have method
Enum can not extends class but can implement interface.
 */
public class EnumTest {

	public static void main(String[] args) {
		Months[] months=Months.values();
		for(Months month:months) {
			System.out.println("Name "  + month.name()+" Value" +month.ordinal());
		}

	}

}

enum Months {
	JAN, FEB, MARCh

}
