package com.BasicProgramming.JavaConcept;

class P {

}

class Q extends P {

}

class R extends Q {
}

public class MostSpecificInstanceCalling {
	public void test(Q obj) {
		System.out.println("B");
	}

	public void test(R obj) {
		System.out.println("C");
	}

	public static void main(String[] args) {
		MostSpecificInstanceCalling obj = new MostSpecificInstanceCalling();
		obj.test(null);
	}
}
