package com.BasicProgramming.JavaConcept;

public class CompositionDemo {

	public static void main(String[] args) {
		Honda hn=new Honda();
		hn.setColor("Black");
		hn.setWheels(2);
		hn.bikeFeatures();
	}

}

class Bike{
	private String color;
	private int wheels;
	
	public void bikeFeatures(){
		System.out.println("Color :"+" "+color +" " +"wheels :" +wheels);
	}
	public void setColor(String color){
		this.color=color;
	}
	public void setWheels(int wheels){
		this.wheels=wheels;
	}
}

class Honda extends Bike{
	
	public void setStart(){
		BikeEngine bkEngine=new BikeEngine();
		bkEngine.start();
	}
}

class BikeEngine{
	
	public void start(){
		System.out.println("Insidebike engine class and engine is started");
	}
	public void stop(){
		System.out.println("Inside bike engine class and engine is stopped");
	}
}