package com.BasicProgramming.JavaConcept;

class A {
	void method_class() {

		class B {
			void display() {
				System.out.println("Inside Class B");
			}
		}
		// B b=new B();
		// b.display();
	}
}

public class Class_inside_Method {

	public static void main(String[] args) {
		//Withot creating instance of class B we can call its method
		A a=new A();
		a.method_class();
		// Or by this way we can have call class b Methods

		/*
		 * B b=new B(); b.display();
		 */

	}

}
