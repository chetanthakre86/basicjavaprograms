
package com.BasicProgramming.JavaConcept;

public class PrivateKeyword {

	public static void main(String[] args) {

		// Parent1 p = new Child1();
		//Child1 c = new Child1();
		// Parent1 p1 = new Parent1();
		// p1.methodPrivate(); //(Override)Calling to private method not
		// possible
		// c.methodPrivate1(); //Calling to private method not possible
		OuterClass outerClass = new OuterClass();
		outerClass.toCreateInstanceOfInnerClass();
		
	}

}

class Parent1 {
	Parent1() {
		finalNorm();
		methodPrivate();
		norm();
	}

	final void finalNorm() {

		System.out.println("To ovrride final method in java :parent class");
	}

	private void methodPrivate() {

		System.out.println("To override private method in java;Parent class");

	}

	public void norm() {
		System.out.println("To ovrride public method in java");

	}
}

class Child1 extends Parent1 {
	Child1() {
		methodPrivate();

	}

	/*
	 * final void finalNorm(){
	 * System.out.println("Final method in Child class"); }
	 */
	private void methodPrivate() {
		System.out.println("Private method in a child class. Can not called even using Run time polymorphism concept");
	}

	private void methodPrivate1() {

		System.out.println("To override private method in java;Parent class");
	}

	public void norm() {
		System.out.println("Override method in a child class");

	}
}

// Outer class can not be "Private". Only inner class can be private. private
// and non private method of outer class can be called inside the inner private
// class(Only inside the method). Inner private class is not visible outside the
// outer class.
// We can not call private method even by object of his own class.
//To call private class have to create object of inner class inside the outer class of non-private method 
class OuterClass {
	private int a = 10;

	private void outerClassPrivateMethod() {
		System.out.println("Inside the Outer Class private method");
	}

	void outerClassNonPrivateMethod() {
		System.out.println("Inside the Outer Class non-private method");
	}
	void toCreateInstanceOfInnerClass(){
		System.out.println("It will create instacne of inner class");
		InnerClassPrivate innerClass = new InnerClassPrivate();
		innerClass.innerClassMethod1();
	}

	private class InnerClassPrivate {
		InnerClassPrivate() {
			//innerClassMethod1();
		}

		private void innerClassMethod1() {
			System.out.println("Inside the Inner Class non private method");
			a = 20;
			System.out.println("Outer class private variable :"+a);
			outerClassPrivateMethod();
			outerClassNonPrivateMethod();
		}

		
	}

}