package com.BasicProgramming.JavaConcept;

@FunctionalInterface
public interface InterfaceLambda {
	public void draw();

}

interface NoParameter {
	public String say();
	//public int no();
}

interface MultiParameter {
	int add(int a, int b);
}

interface ReferenceConstructor{
	ConstructorReference say(String s);
}
