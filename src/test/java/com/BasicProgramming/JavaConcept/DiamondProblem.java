package com.BasicProgramming.JavaConcept;

public class DiamondProblem {

	public static void main(String[] args) {
		DiamondProblem dProblem = new DiamondProblem();
		ImplementsAll impAll = new ImplementsAll();
		impAll.show();
	}

}

interface PT1 {
	default void show() {
		System.out.println("Method inside the interface PT1");
	}
}

interface PT2 {
	default void show() {
		System.out.println("method inside the interface PT2");
	}

}

class ImplementsAll implements PT1, PT2 {

	public void show() {
		PT2.super.show();
		PT1.super.show();
	}

}