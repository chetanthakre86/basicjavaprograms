package com.BasicProgramming.JavaConcept;

public class LambdaClass {

	public static void main(String[] args) {
		// Anonymous Class
		ChildAnnonymous childAnn = new ChildAnnonymous() {

			@Override
			public void show() {
				System.out.println("Inside the Anonymous class ");
			}

			@Override
			public void healthy() {
				// TODO Auto-generated method stub

			}

		};
		childAnn.show();

		// Lambda Expression
		InterfaceLambda lambda = () -> {
			System.out.println("Inside Lambda");
		};
		lambda.draw();

		// Lambda without parameter but with return
		NoParameter noParam = () -> {
			return "Say Something";
		};
		System.out.println(noParam.say());

		// Lambda with parameter
		MultiParameter multiParam = (a, b) -> (a + b);
		System.out.println(multiParam.add(10, 20));

		// Lambda with parameter with return keyword
		MultiParameter multiParam1 = (int a, int b) -> {
			return (a * b);

		};
		System.out.println(multiParam1.add(100, 200));

	}
}
