package com.BasicProgramming.Runners;

import java.util.Scanner;

import com.BasicProgramming.Soulutions.EncapsulationConcept;

/**
 * @author Sanyu
 *
 */
public class OOPSConcept {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Please enter the class name");
		String className = sc.nextLine();
		ToInstantiateClass instanceClass = new ToInstantiateClass();
		instanceClass.createInstance(className);

	}

}

/*
 * abstract class UpsProgrammingConcept {
 * 
 * }
 */

class ToInstantiateClass {

	public UpsProgrammingConcept createInstance(String className) {
		if (className == null) {
			return null;
		} else if (className.equalsIgnoreCase("Encapsulation")) {
			return new EncapsulationConcept();
		}
		return null;

	}
}
