package com.BasicProgramming.Runners;

import java.util.Scanner;

import com.BasicProgramming.Soulutions.SortingUisngMethodReference;
import com.BasicProgramming.Soulutions.SortingUsingComparable;
import com.BasicProgramming.Soulutions.SortingUsingComparator;
import com.BasicProgramming.Soulutions.SortingUsingLambdaExpression;
import com.BasicProgramming.Soulutions.SortingUsingAnonymousClass;

public class SortingUsingInterface {
	public static void main(String[] args) {
		Initializer sorting = new Initializer();
		Scanner sc = new Scanner(System.in);
		System.out.println(
				"Please enter the class name which wants to instantiate like Comparator,Comparable,Comparable1");
		String returnType = sc.nextLine();
		sorting.collectionInitializer(returnType);

	}
}

class Initializer {
	public AbstractSortingCollection collectionInitializer(String className) {
		if (className == null) {
			return null;
		} else if (className.equalsIgnoreCase("Comparator")) {
			return new SortingUsingComparator();
		} else if (className.equalsIgnoreCase("Comparable")) {
			return new SortingUsingComparable();
		} else if (className.equalsIgnoreCase("Method")) {
			return new SortingUisngMethodReference();
		} else if (className.equalsIgnoreCase("Anonymous")) {
			return new SortingUsingAnonymousClass();
		} else if (className.equalsIgnoreCase("LambdaExpression")) {
			return new SortingUsingLambdaExpression();
		}
		return null;
	}
}