package com.BasicProgramming.Runners;

import java.util.Scanner;

import com.BasicProgramming.Runners.ArrayManipulation;
import com.BasicProgramming.Soulutions.AddingAndRemovingInteger;
import com.BasicProgramming.Soulutions.ArrayMapConversion;
import com.BasicProgramming.Soulutions.ArrayPrograms;
import com.BasicProgramming.Soulutions.MultiDimensionalArray;

/**
 * @author Sanyu Factrory Method Design pattern is used to manipulate
 *
 */
public class ArrayPublic {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		InstantiateArrayClass instanceArray = new InstantiateArrayClass();
		System.out.println(
				"Please enter array class name to instantiate like parray,AddRemove,ArrayToMap,MultiArraySorting");

		// To instantiate required class
		String arrayClassName = sc.nextLine();
		instanceArray.getArrayClassInstance(arrayClassName);

	}

}

/*
 * abstract class ArrayManipulation {
 * 
 * }
 */
class InstantiateArrayClass {

	public ArrayManipulation getArrayClassInstance(String arrayClassName) {
		if (arrayClassName == null) {
			return null;
		} else if (arrayClassName.equalsIgnoreCase("Parray")) {
			return new ArrayPrograms();
		} else if (arrayClassName.equalsIgnoreCase("AddRemove")) {
			return new AddingAndRemovingInteger();
		} else if (arrayClassName.equalsIgnoreCase("ArrayToMap")) {
			return new ArrayMapConversion();
		} else if (arrayClassName.equalsIgnoreCase("MultiArraySorting")) {
			return new MultiDimensionalArray();
		}

		return null;

	}
}
