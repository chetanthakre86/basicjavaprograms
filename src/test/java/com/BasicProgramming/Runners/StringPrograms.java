package com.BasicProgramming.Runners;

import java.util.Scanner;
import com.BasicProgramming.Soulutions.AddRemoveStringElements;
import com.BasicProgramming.Soulutions.AdditionOfNosFromString;
import com.BasicProgramming.Soulutions.ReverseString;
import com.BasicProgramming.Soulutions.StringLengthUsingException;
import com.BasicProgramming.Soulutions.StringSorting;
import com.BasicProgramming.Soulutions.DuplicateWords;

/*
 * @author Sanyu
  */
/*abstract class AbstractStringProgram {

}*/
public class StringPrograms {

	public static void main(String[] args) {
		StringOperations stringOp = new StringOperations();
		Scanner sc = new Scanner(System.in);
		System.out.println(
				"Please enter the class name which wants to instantiate like addremove,reverse,DupWords,stringAddition,StringSorting,StringLength");
		String returnType = sc.nextLine();
		stringOp.getInstance(returnType);

	}

}

class StringOperations {

	AbstractStringProgram getInstance(String returnType) {
		if (returnType == null) {
			return null;
		} else if (returnType.equalsIgnoreCase("AddRemove")) {
			return new AddRemoveStringElements();
		} else if (returnType.equalsIgnoreCase("Reverse")) {
			return new ReverseString();
		} else if (returnType.equalsIgnoreCase("DupWords")) {
			return new DuplicateWords();
		} else if (returnType.equalsIgnoreCase("stringAddition")) {
			return new AdditionOfNosFromString();
		} else if (returnType.equalsIgnoreCase("StringSorting")) {
			return new StringSorting();
		} else if (returnType.equalsIgnoreCase("StringLength")) {
			return new StringLengthUsingException();
		}
		return null;

	}
}