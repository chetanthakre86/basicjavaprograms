package com.BasicProgramming.Runners;

import java.util.Scanner;

import com.BasicProgramming.Soulutions.HackerRankSolution1;
import com.BasicProgramming.Soulutions.HackerRankSolution2;
import com.BasicProgramming.Soulutions.HackerRankSolution3;
import com.BasicProgramming.Soulutions.HackerRankSolution4;
import com.BasicProgramming.Soulutions.HackerRankSolution5;
import com.BasicProgramming.Soulutions.HackerRankSolution6;
import com.BasicProgramming.Soulutions.HackerRankSolution7;
import com.BasicProgramming.Soulutions.HackerRankSolution8;
import com.BasicProgramming.Soulutions.HackerRankSolution9;

/**
 * @author Sanyu
 *
 */
public class HackerRankSolutions {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Please enter the class name");
		String className = sc.nextLine();
		InitializerClass instanceClass = new InitializerClass();
		instanceClass.getInstance(className);

	}

}

/*
 * abstract class Hacker{
 * 
 * }
 */

class InitializerClass {

	public AbstractHacker getInstance(String className) {
		if (className == null) {
			return null;
		} else if (className.equalsIgnoreCase("Solution1")) {
			return new HackerRankSolution1();
		} else if (className.equalsIgnoreCase("Solution2")) {
			return new HackerRankSolution2();
		} else if (className.equalsIgnoreCase("Solution3")) {
			return new HackerRankSolution3();
		} else if (className.equalsIgnoreCase("Solution4")) {
			return new HackerRankSolution4();
		} else if (className.equalsIgnoreCase("Solution5")) {
			return new HackerRankSolution5();
		} else if (className.equalsIgnoreCase("Solution6")) {
			return new HackerRankSolution6();
		} else if (className.equalsIgnoreCase("Solution7")) {
			return new HackerRankSolution7();
		} else if (className.equalsIgnoreCase("Solution8")) {
			return new HackerRankSolution8();
		} else if (className.equalsIgnoreCase("Solution9")) {
			return new HackerRankSolution9();
		}
		return null;
	}
}
