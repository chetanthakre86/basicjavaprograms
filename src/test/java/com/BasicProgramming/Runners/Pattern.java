package com.BasicProgramming.Runners;

import java.util.Scanner;

import com.BasicProgramming.Soulutions.CentarNumericPattern;
import com.BasicProgramming.Soulutions.CenterStarPattern;
import com.BasicProgramming.Soulutions.LeftNumericPattern;
import com.BasicProgramming.Soulutions.LeftstarPattern;
import com.BasicProgramming.Soulutions.RightNumericPattern;
import com.BasicProgramming.Soulutions.RightstarPattern;

public class Pattern {

	public static void main(String[] args) {
		PatternDecider pattern = new PatternDecider();
		System.out.println("Please enter required pattern type. rstar/lstar/rnumeric/lnumeric");
		Scanner sc = new Scanner(System.in);
		String type = sc.nextLine();
		System.out.println(type + " : is chosen.");
		pattern.decidePattern(type);
	}
}

/*
 * abstract class StarAndNumericPattern {
 * 
 * int noOfRows;
 * 
 * StarAndNumericPattern() {
 * System.out.println("Please Enter the no of rows you want"); Scanner sc = new
 * Scanner(System.in); noOfRows = sc.nextInt(); }
 * 
 * // abstract public void printPattern(); }
 */

class PatternDecider {

	public StarAndNumericPattern decidePattern(String patternType) {

		if (patternType == null) {
			return null;
		}

		if (patternType.equalsIgnoreCase("LStar")) {
			return new LeftstarPattern();
		} else if (patternType.equalsIgnoreCase("RStar")) {
			return new RightstarPattern();
		} else if (patternType.equalsIgnoreCase("LNumeric")) {
			return new LeftNumericPattern();
		} else if (patternType.equalsIgnoreCase("RNumeric")) {
			return new RightNumericPattern();
		} else if (patternType.equalsIgnoreCase("cstar")) {
			return new CenterStarPattern();
		} else if (patternType.equalsIgnoreCase("cNPattern")) {
			return new CentarNumericPattern();
		}
		return null;

	}
}
